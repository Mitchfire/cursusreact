import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';
import { NavLink } from 'react-router-dom';


class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			headerText: "Welcome to React!",
			contentText: "In this lecture, we will go over the Component"
		};
	}
	render() {
		return (
			<div className="App">
				<ul>
                  <li><NavLink activeStyle={{color: 'green'}} to="/home">Home</NavLink></li>
                  <li><NavLink activeStyle={{color: 'green'}} to="/about">About</NavLink></li>
                  <li><NavLink activeStyle={{color: 'green'}} to="/contact">Contact</NavLink></li>
                  <li><NavLink activeStyle={{color: 'green'}} to="/blog">Blog</NavLink></li>
				</ul>
			{this.props.children}
			</div>
		);
	}
}


class Header extends Component {
	render() {
		return (
			<div className="App-header">
			<img src={logo} className="App-logo" alt="logo" />
			<h2>Welcome to React</h2>
			</div>
		);
	}
}

class Content extends Component {
	render() {
		return (
			<div className="App-intro">
				<h1>Components routes!</h1>
				<p>In this lecture, we will go over the Forms in React</p>	
			</div>
		);
	}
}












export default App;
