import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';

class Blog extends Component {
	render(){
		return(
          <div>
		<h2>Blog</h2>
          <ul>
            <li><NavLink activeStyle={{color: 'green'}} to="/blog/123">Blog 1</NavLink></li>
            <li><NavLink activeStyle={{color: 'green'}} to="/blog/234">Blog 2</NavLink></li>
            <li><NavLink activeStyle={{color: 'green'}} to="/blog/456">Blog 3</NavLink></li>
          </ul>
          {this.props.children}
          </div>
		);
	}
}

export default Blog;